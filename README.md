# Local Oye App

### Installation

* Install pip (using a virtual environment is recommended)
* Install MySql
* Install the required dependencies from **requirements.txt** file with pip
```
pip install -r requirements.txt
```
* Clone the project
* Create a superuser and/or users for the application 
```python manage.py createsuperuser```

    **NOTE:** Change the DB settings in settings.py with yours

```
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'localOye',
        'HOST': 'localhost',
        'PORT': '3306',
        'USER': 'root',
        'PASSWORD': 'yourPassword',
    }
}
```

* Run project using

```
python manage.py runserver
```

* web version available at localhost:8000

* admin panel available at localhost:8000/admin

* APIs : 

    /api/categories : to get list of categories
    
    /api/<category_id>/questions : to get questions for a category
  