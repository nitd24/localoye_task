from django.conf.urls import patterns, include, url
from django.contrib import admin


urlpatterns = patterns('',
    #questions related
    url(r'^', include('questions.urls', namespace='questions')),

    #admin
    url(r'^admin/', include(admin.site.urls)),
)
