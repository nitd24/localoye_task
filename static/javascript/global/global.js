$(function() {
    var target_element = '#id_categories';
    get_categories_api(target_element);
});


function get_categories_api(target_element) {
    $.ajax({
        url: "/api/categories",
        type : "GET",
        dataType: "json",
        success: function(d) {
            categories = eval(d.categories);
            for (i=0; i<categories.length; i++) {
                var html = '<li class="active"><a href="/' + categories[i].pk + '/questions">' + categories[i].fields.category + '</a></li>'
                $(target_element).append(html);
            };
        },
        error: function(data) {
            error = true;
        },
    });
};