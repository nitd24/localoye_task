from django.contrib import admin

from questions.models import Category, Question, Option

admin.site.register(Category)

#for specifying options for single/multiselect questions
class OptionInline(admin.TabularInline):
    model = Option
    extra = 3

    class Media:
        js = ("javascript/resources/jquery.min.js","javascript/admin/question_add.js",)


class QuestionAdmin(admin.ModelAdmin):
    inlines = [OptionInline]
    list_display = ('question', 'question_order')
    list_filter = ('question_order',)


admin.site.register(Question, QuestionAdmin)
