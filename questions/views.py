from django.shortcuts import render
from django.http import JsonResponse
import json
from django.core import serializers

from questions.models import Category, Question

# dummy webpage
def index(request):
    context = {}
    return render(request, 'global/index.html', context)


def get_questions_by_category(request, category_id):
    category = Category.objects.filter(id=category_id)
    questions = Question.objects.select_related('option').filter(category=category, is_active=True).order_by('question_order')
    context = {'questions': questions}
    return render(request, 'questions/questions.html', context)


#--- API to get categories
def get_categories_api(request):
    categories = Category.objects.filter(is_active=True)
    categories = serializers.serialize('json', categories)
    context = {'categories': categories}
    return JsonResponse(context)
    
# --- API to get questions(and options) for a category
def get_questions_by_category_api(request, category_id):
    category = Category.objects.filter(id=category_id)
    questions = Question.objects.select_related('option').filter(category=category, is_active=True).order_by('question_order')
    #--- Send options along with the questions
    data = {'questions': []}
    for question in questions:
        q = {'id': question.id,'question': question.question, 'category': question.category.id, 'question_order': question.question_order, 'answer_type': question.answer_type}
        if question.answer_type != question.TEXT_TYPE_ANSWER:
            q['options'] = []
            for option in question.option_set.all():
                o = {'id': option.id, 'option': option.option}
                q['options'].append(o)
        data['questions'].append(q)
    data = json.dumps(data)
    context = {'data': data}
    return JsonResponse(context)