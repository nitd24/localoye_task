from django.conf.urls import patterns, url


urlpatterns = patterns('',
        url(r'^$', 'questions.views.index', name='index'),
        #--- Pages
        url(r'^(?P<category_id>\d+)/questions$', 'questions.views.get_questions_by_category', name='get_questions_by_category'),
        #--- API urls
        url(r'^api/categories$', 'questions.views.get_categories_api', name='get_categories_api'),
        url(r'^api/(?P<category_id>\d+)/questions$', 'questions.views.get_questions_by_category_api', name='get_questions_by_category_api'),
    )