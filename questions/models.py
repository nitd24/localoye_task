import datetime

from django.db import models

#Categories model
class Category(models.Model):
    category = models.CharField(max_length=200)
    created_on = models.DateTimeField(default=datetime.datetime.now)
    is_active = models.BooleanField(default=True)
    
    class Meta:
        verbose_name_plural = 'categories'
    
    def __unicode__(self):
        return self.category

#Questions model
class Question(models.Model):
    #types of answer : text, single select, multiselect
    MULTI_OPTION_TYPE_ANSWER = 'mutli_option'
    SINGLE_OPTION_TYPE_ANSWER = 'single_option'
    TEXT_TYPE_ANSWER = 'text'
    
    ANSWER_TYPE_CHOICES = [(MULTI_OPTION_TYPE_ANSWER,'Multiple options'),(SINGLE_OPTION_TYPE_ANSWER,'Single option'),(TEXT_TYPE_ANSWER,'Textual answer')]
    
    question = models.CharField(max_length=1000)
    category = models.ForeignKey(Category)
    question_order = models.IntegerField()
    answer_type = models.CharField(max_length=20, choices=ANSWER_TYPE_CHOICES, blank=True)
    created_on = models.DateTimeField(default=datetime.datetime.now)
    is_active = models.BooleanField(default=True)
    
    def __unicode__(self):
        return self.question
    
    def natural_key(self):
        return (self.question,) + self.option.natural_key()

#Options model
class Option(models.Model):
    question = models.ForeignKey(Question)
    option = models.CharField(max_length=200)
    created_on = models.DateTimeField(default=datetime.datetime.now)
    is_active = models.BooleanField(default=True)
    
    def __unicode__(self):
        return self.option



#Answers model for single/multi select
class AnsweredOption(models.Model):
    question = models.ForeignKey(Question)
    option = models.ForeignKey(Option)
    created_on = models.DateTimeField(default=datetime.datetime.now)
    is_active = models.BooleanField(default=True)
    
#Answers model for text
class Answer(models.Model):
    question = models.OneToOneField(Question)
    answer = models.CharField(max_length=200)
    created_on = models.DateTimeField(default=datetime.datetime.now)
    is_active = models.BooleanField(default=True)
    
    def __unicode__(self):
        return self.answer